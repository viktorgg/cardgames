package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.List;

public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };
    private List<PlayingCard> allCards = new ArrayList<>();
    private final int amountOfSuit = 4;
    private final int amountOfCards = 13;
    int randomNum;

    public DeckOfCards(){
        for (int i = 0; i < amountOfSuit; i++){
            for (int j = 1; j < amountOfCards + 1; j++){
                allCards.add(new PlayingCard(suit[i], j, "PNG-cards-1.3/" + suit[i] + j + ".png"));
            }
        }
    }

    public List<PlayingCard> getAllCards(){
        return allCards;
    }

    public ArrayList<PlayingCard> dealHand(int amountOfCards){
        ArrayList<PlayingCard> handOfCards = new ArrayList<>();
        for (int i = 0; i < amountOfCards; i++){
            randomNum = (int)(Math.random() * allCards.size());
            handOfCards.add(allCards.get(randomNum));
            allCards.remove(randomNum);
        }
        return handOfCards;
    }
}
