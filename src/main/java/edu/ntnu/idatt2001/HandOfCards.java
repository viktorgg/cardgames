package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class HandOfCards {

    private ArrayList<PlayingCard> yourHand;

    public HandOfCards(ArrayList<PlayingCard> yourHand){
        this.yourHand = yourHand;
    }

    public boolean checkForFlush(){
        char firstCard = yourHand.get(0).getSuit();
        return yourHand.stream().allMatch(e -> e.getSuit() == firstCard);
    }

    public int countOfTheFaces(){
        return yourHand.stream()
                .mapToInt(PlayingCard::getFace)
                .sum();
    }

    public ArrayList<PlayingCard> allOfOneSuit(char suit){
        return (ArrayList<PlayingCard>) yourHand.stream().filter(card ->
                card.getSuit() == 'H').collect(Collectors.toList());
    }

    public boolean checkForQueenOfSpades(){
        return yourHand.stream().anyMatch(card -> card.getAsString().equals("S12"));
    }

    public ArrayList<PlayingCard> getYourHand() {
        return yourHand;
    }
}