package edu.ntnu.idatt2001;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.geometry.Pos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
public class GUI extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception{
        Label label = new Label("Card game");
        label.setTextFill(Color.BROWN);
        label.setStyle("-fx-font: 60 arial;" +
                "-fx-font-weight: bold;");
        VBox parent = new VBox(label);

        HBox handBox = new HBox(new Label("Your hand: "));
        handBox.setAlignment(Pos.CENTER);
        Text yourHand = new Text();
        handBox.getChildren().add(yourHand);
        parent.getChildren().add(handBox);
        handBox.setStyle("-fx-font: 16 arial;");

        HBox sumBox = new HBox(new Label("Sum: "));
        sumBox.setAlignment(Pos.CENTER);
        Text sumHand = new Text();
        sumBox.getChildren().add(sumHand);
        parent.getChildren().add(sumBox);
        sumBox.setStyle("-fx-font: 16 arial;");

        HBox flushBox = new HBox(new Label("Flush: "));
        flushBox.setAlignment(Pos.CENTER);
        Text flushHand = new Text();
        flushBox.getChildren().add(flushHand);
        parent.getChildren().add(flushBox);
        flushBox.setStyle("-fx-font: 16 arial;");

        HBox queenBox = new HBox(new Label("Queen of spades: "));
        queenBox.setAlignment(Pos.CENTER);
        Text queenHand = new Text();
        queenBox.getChildren().add(queenHand);
        parent.getChildren().add(queenBox);
        queenBox.setStyle("-fx-font: 16 arial;");

        HBox heartBox = new HBox(new Label("All hearts in your hand: "));
        heartBox.setAlignment(Pos.CENTER);
        Text heartHand = new Text();
        heartBox.getChildren().add(heartHand);
        parent.getChildren().add(heartBox);
        heartBox.setStyle("-fx-font: 16 arial;");

        HBox imageBox = new HBox();
        imageBox.setAlignment(Pos.CENTER);
        parent.getChildren().add(imageBox);
        imageBox.setMinHeight(200);
        String imageUrl = "poker.jpg";
        imageBox.setStyle(
                "    -fx-border-radius: 20;" +
                        "    -fx-background-radius: 20;" +
                        "    -fx-padding: 5;" +
                        "    -fx-opacity: 0.5;" +
                        "    -fx-border-color: black;" +
                        "-fx-background-image: url(" + imageUrl + ");" +
                        "-fx-background-repeat: stretch;" +
                        "-fx-background-size: 1000 700;" +
                        "-fx-background-position: center center;");
        imageBox.setMinWidth(300);
        imageBox.setMaxWidth(450);

        Button button = new Button("Deal hand!");
        button.setStyle(
                "-fx-text-fill: white;\n" +
                        "    -fx-background-color: brown;\n" +
                        "    -fx-border-radius: 20;\n" +
                        "    -fx-background-radius: 20;\n" +
                        "    -fx-padding: 5;" +
                        "    -fx-font-size: 20px;" +
                        "    -fx-border-insets: 5px;" +
                        "    -fx-background-insets: 5px;" +
                        "    -fx-border-color: black;");
        button.setMaxWidth(200);

        button.setOnAction(actionEvent -> {
            DeckOfCards deckOfCards = new DeckOfCards();
            HandOfCards handOfCards = new HandOfCards(deckOfCards.dealHand(5));
            imageBox.setStyle(
                    "    -fx-border-radius: 20;\n" +
                            "    -fx-background-radius: 20;\n" +
                            "    -fx-padding: 5;" +
                            "    -fx-opacity: 1;" +
                            "    -fx-border-color: black;" +
                            "-fx-background-image: url(" + imageUrl + ");" +
                            "-fx-background-repeat: stretch;" +
                            "-fx-background-size: 1000 700;" +
                            "-fx-background-position: center center;");

            yourHand.setText(printCards(handOfCards.getYourHand()));
            sumHand.setText("" + handOfCards.countOfTheFaces());
            flushHand.setText("" + handOfCards.checkForFlush());
            queenHand.setText(("" + handOfCards.checkForQueenOfSpades()));
            heartHand.setText(printCards(handOfCards.allOfOneSuit('H')));
            imageBox.getChildren().clear();
            Image image = null;
            for (PlayingCard card : handOfCards.getYourHand()){
                ImageView imageView  = importImage(card.getImage(), 80, 110, 10,10);
                imageBox.getChildren().add(imageView);
            }
        });
        button.setAlignment(Pos.CENTER);
        parent.getChildren().add(button);

        Button exitButton = new Button("Exit");
        exitButton.setOnAction(actionEvent -> {
            primaryStage.close();
        });
        exitButton.setAlignment(Pos.CENTER);
        parent.getChildren().add(exitButton);
        parent.setAlignment(Pos.CENTER);

        parent.setStyle("-fx-background-color: transparent");

        Scene scene = new Scene(parent, 800, 600);

        scene.setFill(new RadialGradient(
                0, 0, 0, 0, 1, true,    //sizing
                CycleMethod.NO_CYCLE,                       //cycling
                new Stop(0, Color.web("#013337")),    //colors
                new Stop(1, Color.web("#5865F2")))

        );

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public String printCards(ArrayList<PlayingCard> cards){
        StringBuilder text = new StringBuilder();
        for (PlayingCard card : cards){
            text.append(card.getAsString()).append(" - ");
        }
        return text.toString();
    }

    public ImageView importImage(String path, double sizeH, double sizeV, double XCoordinates, double YCoordinates){
        Image image = new Image(path, sizeH, sizeV, false, false);
        ImageView imageView = new ImageView(image);
        imageView.setX(XCoordinates);
        imageView.setY(YCoordinates);
        return imageView;
    }
}