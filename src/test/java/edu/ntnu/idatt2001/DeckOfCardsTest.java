package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    DeckOfCards deckOfCards = new DeckOfCards();
    @Test
    void CheckIfConstructorMakes52Cards() {
        int expected = 52;
        int actual = deckOfCards.getAllCards().size();
        assertEquals(expected, actual);
    }

    @Test
    void CheckThatDealHandReturnsTwoCardsInArrayList() {
        int expected = 2;
        int amountOfCardsDealed = 2;
        int actual = deckOfCards.dealHand(amountOfCardsDealed).size();
        assertEquals(expected, actual);

    }
}