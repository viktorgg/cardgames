package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class HandOfCardsTest {

    List<PlayingCard> testHand = Arrays.asList(new PlayingCard('S', 10, "url"), new PlayingCard('S', 5, "url"));
    HandOfCards handOfCards = new HandOfCards((ArrayList<PlayingCard>) testHand);

    @Test
    void checkForFlush() {
        assertTrue(handOfCards.checkForFlush());
    }

    @Test
    void countOfTheFaces() {
        int expected = 15;
        int actual = handOfCards.countOfTheFaces();
        assertEquals(expected, actual);
    }

    @Test
    void allOfOneSuit() {
        assertTrue(handOfCards.allOfOneSuit('H').isEmpty());
        assertTrue(handOfCards.allOfOneSuit('S').size() == 2);
    }

    @Test
    void checkForQueenOfSpades() {
        assertFalse(handOfCards.checkForQueenOfSpades());
    }

    @Test
    void getYourHand() {
        List<PlayingCard> expected = Arrays.asList(new PlayingCard('S', 10, "url"), new PlayingCard('S', 5, "url"));
        List<PlayingCard> actual = handOfCards.getYourHand();
        assertEquals(expected.get(0), actual.get(0));
        assertEquals(expected.get(1), actual.get(1));
    }
}