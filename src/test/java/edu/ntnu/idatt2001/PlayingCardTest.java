package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {

    PlayingCard playingCard = new PlayingCard('S', 10, "C:\\Users\\vikto\\Downloads\\PNG-cards-1.3\\PNG-cards-1.3\\" + 'S' + 10 + ".png");

    @Test
    void Contructor() {
        String expected = "S10";
        String actual = playingCard.getAsString();
        assertEquals(expected, actual, "The expected should be S10 because of the concatenation with S and 10");
    }

    @Test
    void getAsString() {
        String expected = "S10";
        String actuall = playingCard.getAsString();
        assertEquals(expected, actuall);
    }

    @Test
    void getSuit() {
        char expected = 'S';
        char actuall = playingCard.getSuit();
        assertEquals(expected, actuall);
    }

    @Test
    void getFace() {
        int expected = 10;
        int actuall = playingCard.getFace();
        assertEquals(expected, actuall);
    }

    @Test
    void getImage() {
        String expected = "C:\\Users\\vikto\\Downloads\\PNG-cards-1.3\\PNG-cards-1.3\\S10.png";
        String actual = playingCard.getImage();
        assertEquals(expected, actual);
    }
}